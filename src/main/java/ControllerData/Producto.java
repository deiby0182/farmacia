package ControllerData;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import Models.ConexionDB;

public class Producto {
    
    private int id;
    private String nombre;
    private double temperatura;
    private double valorBase;
    private double costo;

    public Producto(int id, String nombre, double temperatura, double valorBase, double costo) {
        this.nombre = nombre;
        this.id = id;
        this.temperatura = temperatura;
        this.valorBase = valorBase;
        this.costo = costo;
    }
    
    public Producto(){
        
    }

    public double getCosto() {
        return costo;
    }

    public void setCosto(double costo) {
        this.costo = costo;
    }   
    
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String Nombre) {
        this.nombre = Nombre;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getTemperatura() {
        return temperatura;
    }

    public void setTemperatura(double temperatura) {
        this.temperatura = temperatura;
    }

    public double getValorBase() {
        return valorBase;
    }

    public void setValorBase(double ValorBase) {
        this.valorBase = ValorBase;
    }
    
    public double calcularCostoDeAlmacenamiento(){
        if(getTemperatura() > 21){
            return getValorBase() * 1.1;
        }        
        else{
            return getValorBase() * 1.2;
        }       
    }   
    
    @Override
    public String toString() {
        return this.getClass().getName() + "{" + "nombre=" + nombre + ", temperatura=" + temperatura + ", valorBase=" + valorBase + ", Costo=" + costo +'}';
    }
    
    public List<Producto> listarProductos() {
        List<Producto> listaProductos = new ArrayList<>();
        ConexionDB conexion = new ConexionDB();
        String sql = "SELECT * FROM Productos;";
        try {
            ResultSet rs = conexion.consultarDB(sql);
            Producto p;
            while (rs.next()) {
                p = new Producto();
                p.setId(rs.getInt("id"));
                p.setNombre(rs.getString("nombre"));
                p.setTemperatura(rs.getDouble("temperatura"));
                p.setValorBase(rs.getDouble("valorbase")); 
                p.setCosto(rs.getDouble("costo"));                
                listaProductos.add(p);
            }
        } 
        catch (SQLException ex) {
            System.out.println("Error al listar productos:" + ex.getMessage());
        } 
        finally {
            conexion.cerrarConexion();
        }
        return listaProductos;
    }
    
    public boolean guardarProducto() {
        ConexionDB conexion = new ConexionDB();
        String sql = "INSERT INTO Productos(nombre,temperatura,valorbase,costo)"
                + "VALUES('" + this.nombre + "'," + this.temperatura + ",'" + this.valorBase + "'," + this.costo + ");";
        if (conexion.setAutoCommitDB(false)) {
            
            if (conexion.insertarDB(sql)) {
                conexion.commitDB();
                conexion.cerrarConexion();
                return true;
            } 
            else {
                conexion.rollbackDB();
                conexion.cerrarConexion();
                return false;
            }
        }
        else {
            conexion.cerrarConexion();
            return false;
        }
    }

    public boolean actualizarProducto() {
        ConexionDB conexion = new ConexionDB();
        String sql = "UPDATE Productos SET nombre='" + this.nombre + "',temperatura=" + this.temperatura + ",valorbase='" + this.valorBase + "',costo=" + this.costo + " WHERE id=" + this.id + ";";
        if (conexion.setAutoCommitDB(false)) {
            if (conexion.actualizarDB(sql)) {
                conexion.commitDB();
                conexion.cerrarConexion();
                return true;
            } 
            else {
                conexion.rollbackDB();
                conexion.cerrarConexion();
                return false;
            }
        } 
        else {
            conexion.cerrarConexion();
            return false;
        }
    }

    public boolean eliminarProducto() {
        ConexionDB conexion = new ConexionDB();
        String sql = "DELETE FROM Productos WHERE id=" + this.id + ";";
        if (conexion.setAutoCommitDB(false)) {
            if (conexion.actualizarDB(sql)) {
                conexion.commitDB();
                conexion.cerrarConexion();
                return true;
            } 
            else {
                conexion.rollbackDB();
                conexion.cerrarConexion();
                return false;
            }
        } 
        else {
            conexion.cerrarConexion();
            return false;
        }
    }    
    
}
