
package ControllerData;

import Models.ConexionDB;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class Farmacia {
    
    private int id;
    private double nit;
    private String nombre;
    private String direccion;
    private String telefono;

    public Farmacia(int id, double nit, String nombre, String direccion, String telefono) {
        this.id = id;
        this.nit = nit;
        this.nombre = nombre;
        this.direccion = direccion;
        this.telefono = telefono;
    }

    public Farmacia() {
    }
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getNit() {
        return nit;
    }

    public void setNit(double nit) {
        this.nit = nit;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }
    
    @Override
    public String toString() {
        return this.getClass().getName() + "{" + "id=" + id + ", nit=" + nit + ", nombre=" + nombre + ", direccion=" + direccion +", telefono=" + telefono + '}';
    }
    
    public List<Farmacia> ListaFarmacia() {
        List<Farmacia> listaFarmacias = new ArrayList<>();
        ConexionDB conexion = new ConexionDB();
        String sql = "SELECT * FROM Farmacia;";
        try {
            ResultSet rs = conexion.consultarDB(sql);
            Farmacia f;
            while (rs.next()) {
                f = new Farmacia();
                f.setId(rs.getInt("id"));
                f.setNit(rs.getDouble("nit"));
                f.setNombre(rs.getString("nombre"));
                f.setDireccion(rs.getString("direccion")); 
                f.setTelefono(rs.getString("telefono"));                
                listaFarmacias.add(f);
            }
        } 
        catch (SQLException ex) {
            System.out.println("Error al listar productos:" + ex.getMessage());
        } 
        finally {
            conexion.cerrarConexion();
        }
        return listaFarmacias;
    }
    
    public boolean guardarFarmacia() {
        ConexionDB conexion = new ConexionDB();
        String sql = "INSERT INTO Farmacia(nit,nombre,direccion,telefono)"
                + "VALUES(" + this.nit + ",'" + this.nombre + "','" + this.direccion + "','" + this.telefono + "');";
        if (conexion.setAutoCommitDB(false)) {
            
            if (conexion.insertarDB(sql)) {
                conexion.commitDB();
                conexion.cerrarConexion();
                return true;
            } 
            else {
                conexion.rollbackDB();
                conexion.cerrarConexion();
                return false;
            }
        }
        else {
            conexion.cerrarConexion();
            return false;
        }
    }

    public boolean actualizarFarmacia() {
        ConexionDB conexion = new ConexionDB();
        String sql = "UPDATE Farmacia SET nit=" + this.nit + ",nombre='" + this.nombre + "', direccion='" + this.direccion + "', telefono='" + this.telefono + "' WHERE id= " + this.id +";";
        if (conexion.setAutoCommitDB(false)) {
            if (conexion.actualizarDB(sql)) {
                conexion.commitDB();
                conexion.cerrarConexion();
                return true;
            } 
            else {
                conexion.rollbackDB();
                conexion.cerrarConexion();
                return false;
            }
        } 
        else {
            conexion.cerrarConexion();
            return false;
        }
    }

    public boolean eliminarFarmacia() {
        ConexionDB conexion = new ConexionDB();
        String sql = "DELETE FROM Farmacia WHERE id=" + this.id + ";";
        if (conexion.setAutoCommitDB(false)) {
            if (conexion.actualizarDB(sql)) {
                conexion.commitDB();
                conexion.cerrarConexion();
                return true;
            } 
            else {
                conexion.rollbackDB();
                conexion.cerrarConexion();
                return false;
            }
        } 
        else {
            conexion.cerrarConexion();
            return false;
        }
    }
    
}
