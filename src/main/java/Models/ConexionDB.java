package Models;

import java.sql.*;
import java.util.logging.*;

public class ConexionDB {
    
    private String url = "";       
    public  Connection connection = null;
    private Statement stmt = null;
    private ResultSet rs = null;
    
    public ConexionDB(){
        url = "jdbc:sqlite:reto5.db";
        try {
            connection = DriverManager.getConnection(url);
            if(connection != null){
                DatabaseMetaData meta = connection.getMetaData();
                System.out.println("Conexion exitosa a la base de datos" + meta.getDriverName()   );                
            }            
        } 
        catch (Exception e) {
            System.out.println(""+e);
        }        
    }
    public Connection getConnection(){
        return connection;
    }
    
    public void closeConnection(Connection con){
        if (connection != null) {
            try {
                connection.close();
            } 
            catch (SQLException ex) {
                Logger.getLogger(ConexionDB.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    public ResultSet consultarDB(String sentencia) {
        try {
            stmt = connection.createStatement();
            rs = stmt.executeQuery(sentencia);
        } 
        catch (SQLException sqlex) {
            System.out.println(sqlex.getMessage());
        } 
        catch (RuntimeException rex) {
            System.out.println(rex.getMessage());
        } 
        catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return rs;
    }

    public boolean insertarDB(String sentencia) {
        try {
            stmt = connection.createStatement();
            stmt.execute(sentencia);
        } 
        catch (SQLException | RuntimeException sqlex) {
            System.out.println("ERROR RUTINA: " + sqlex);
            return false;
        }
        return true;
    }

    public boolean borrarDB(String sentencia) {
        try {
            stmt = connection.createStatement();
            stmt.execute(sentencia);
        } 
        catch (SQLException | RuntimeException sqlex) {
            System.out.println("ERROR RUTINA: " + sqlex);
            return false;
        }
        return true;
    }


    public boolean actualizarDB(String sentencia) {
        try {
            stmt = connection.createStatement();
            stmt.executeUpdate(sentencia);
        } 
        catch (SQLException | RuntimeException sqlex) {
            System.out.println("ERROR RUTINA: " + sqlex);
            return false;
        }
        return true;
    }

    public boolean setAutoCommitDB(boolean parametro) {
        try {
            connection.setAutoCommit(parametro);
        } 
        catch (SQLException sqlex) {
            System.out.println("Error al configurar el autoCommit " + sqlex.getMessage());
            return false;
        }
        return true;
    }

    public void cerrarConexion() {
        closeConnection(connection);
    }

    public boolean commitDB() {
        try {
            connection.commit();
            return true;
        } catch (SQLException sqlex) {
            System.out.println("Error al hacer commit " + sqlex.getMessage());
            return false;
        }
    }

    public boolean rollbackDB() {
        try {
            connection.rollback();
            return true;
        } catch (SQLException sqlex) {
            System.out.println("Error al hacer rollback " + sqlex.getMessage());
            return false;
        }
    }
    
    
}

