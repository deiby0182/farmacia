package View;

import ControllerData.Farmacia;
import ControllerData.Producto;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class JFrameProductos extends javax.swing.JFrame {

    public JFrameProductos() {
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        Lbl_Nfarmacia = new javax.swing.JLabel();
        Txt_Nombrefarmacia = new javax.swing.JTextField();
        Lbl_IDFarmacia = new javax.swing.JLabel();
        Txt_IDFarmacia = new javax.swing.JTextField();
        Lbl_Direccion = new javax.swing.JLabel();
        Txt_DireccionFarmacia = new javax.swing.JTextField();
        Txt_Telefono = new javax.swing.JTextField();
        jScrollPane2 = new javax.swing.JScrollPane();
        Tb_Farmacia = new javax.swing.JTable();
        btn_ActualizarFarmacia = new javax.swing.JToggleButton();
        btn_GuardarFarmacia = new javax.swing.JToggleButton();
        btn_EliminarFarmacia = new javax.swing.JToggleButton();
        btn_ConsultarFarmacia = new javax.swing.JToggleButton();
        Lbl_IDFarmacia1 = new javax.swing.JLabel();
        Txt_NitFarmacia = new javax.swing.JTextField();
        Lbl_ValorBase1 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        Lbl_Nombre = new javax.swing.JLabel();
        Txt_Nombre = new javax.swing.JTextField();
        Lbl_ID = new javax.swing.JLabel();
        Txt_ID = new javax.swing.JTextField();
        Lbl_Temperatura = new javax.swing.JLabel();
        Txt_Temperatura = new javax.swing.JTextField();
        Lbl_ValorBase = new javax.swing.JLabel();
        Txt_ValorBase = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        Tb_Productos = new javax.swing.JTable();
        btn_Actualizar = new javax.swing.JToggleButton();
        btn_Guardar = new javax.swing.JToggleButton();
        btn_Eliminar = new javax.swing.JToggleButton();
        btn_Consultar = new javax.swing.JToggleButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setLocation(new java.awt.Point(350, 150));

        jTabbedPane1.setBackground(new java.awt.Color(153, 153, 153));
        jTabbedPane1.setForeground(new java.awt.Color(0, 0, 0));
        jTabbedPane1.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));

        jPanel1.setBackground(new java.awt.Color(51, 51, 51));

        Lbl_Nfarmacia.setBackground(new java.awt.Color(255, 255, 255));
        Lbl_Nfarmacia.setFont(new java.awt.Font("Roboto Medium", 3, 14)); // NOI18N
        Lbl_Nfarmacia.setText("Nombre");

        Txt_Nombrefarmacia.setForeground(new java.awt.Color(102, 102, 102));

        Lbl_IDFarmacia.setBackground(new java.awt.Color(255, 255, 255));
        Lbl_IDFarmacia.setFont(new java.awt.Font("Roboto Medium", 3, 14)); // NOI18N
        Lbl_IDFarmacia.setText("ID");

        Txt_IDFarmacia.setForeground(new java.awt.Color(102, 102, 102));

        Lbl_Direccion.setFont(new java.awt.Font("Roboto Medium", 3, 14)); // NOI18N
        Lbl_Direccion.setText("Direccion");

        Txt_DireccionFarmacia.setForeground(new java.awt.Color(102, 102, 102));

        Txt_Telefono.setForeground(new java.awt.Color(102, 102, 102));

        Tb_Farmacia.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        Tb_Farmacia.setFont(new java.awt.Font("Roboto Black", 2, 12)); // NOI18N
        Tb_Farmacia.setForeground(new java.awt.Color(102, 102, 102));
        Tb_Farmacia.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID", "Nit", "Nombre Farmacia", "Direccion", "Telefono"
            }
        ));
        Tb_Farmacia.setGridColor(new java.awt.Color(0, 0, 204));
        jScrollPane2.setViewportView(Tb_Farmacia);

        btn_ActualizarFarmacia.setBackground(new java.awt.Color(204, 204, 0));
        btn_ActualizarFarmacia.setFont(new java.awt.Font("Roboto Black", 2, 12)); // NOI18N
        btn_ActualizarFarmacia.setText("Actualizar");
        btn_ActualizarFarmacia.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btn_ActualizarFarmacia.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_ActualizarFarmaciaActionPerformed(evt);
            }
        });

        btn_GuardarFarmacia.setBackground(new java.awt.Color(51, 204, 0));
        btn_GuardarFarmacia.setFont(new java.awt.Font("Roboto Black", 2, 12)); // NOI18N
        btn_GuardarFarmacia.setText("Guardar");
        btn_GuardarFarmacia.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btn_GuardarFarmacia.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_GuardarFarmaciaActionPerformed(evt);
            }
        });

        btn_EliminarFarmacia.setBackground(new java.awt.Color(255, 0, 0));
        btn_EliminarFarmacia.setFont(new java.awt.Font("Roboto Black", 2, 12)); // NOI18N
        btn_EliminarFarmacia.setText("Eliminar");
        btn_EliminarFarmacia.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btn_EliminarFarmacia.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_EliminarFarmaciaActionPerformed(evt);
            }
        });

        btn_ConsultarFarmacia.setBackground(new java.awt.Color(0, 51, 255));
        btn_ConsultarFarmacia.setFont(new java.awt.Font("Roboto Black", 2, 12)); // NOI18N
        btn_ConsultarFarmacia.setText("Consultar");
        btn_ConsultarFarmacia.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btn_ConsultarFarmacia.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_ConsultarFarmaciaActionPerformed(evt);
            }
        });

        Lbl_IDFarmacia1.setBackground(new java.awt.Color(255, 255, 255));
        Lbl_IDFarmacia1.setFont(new java.awt.Font("Roboto Medium", 3, 14)); // NOI18N
        Lbl_IDFarmacia1.setText("Nit");

        Txt_NitFarmacia.setForeground(new java.awt.Color(102, 102, 102));

        Lbl_ValorBase1.setFont(new java.awt.Font("Roboto Medium", 3, 14)); // NOI18N
        Lbl_ValorBase1.setText("Telefono");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btn_GuardarFarmacia, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(btn_ActualizarFarmacia, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(Lbl_IDFarmacia1, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(Lbl_IDFarmacia, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(Lbl_Nfarmacia, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(Lbl_Direccion, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addComponent(Lbl_ValorBase1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(Txt_IDFarmacia, javax.swing.GroupLayout.PREFERRED_SIZE, 141, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(Txt_NitFarmacia, javax.swing.GroupLayout.PREFERRED_SIZE, 141, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(Txt_Nombrefarmacia, javax.swing.GroupLayout.PREFERRED_SIZE, 141, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(Txt_DireccionFarmacia, javax.swing.GroupLayout.PREFERRED_SIZE, 141, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(Txt_Telefono, javax.swing.GroupLayout.PREFERRED_SIZE, 141, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(btn_EliminarFarmacia, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btn_ConsultarFarmacia, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 487, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(51, 51, 51))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(28, 28, 28)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(8, 8, 8)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(Txt_IDFarmacia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(Lbl_IDFarmacia))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(Txt_NitFarmacia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(Lbl_IDFarmacia1))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(Txt_Nombrefarmacia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(Lbl_Nfarmacia))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(Lbl_Direccion)
                            .addComponent(Txt_DireccionFarmacia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(Txt_Telefono, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(Lbl_ValorBase1))
                        .addGap(58, 58, 58)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btn_ActualizarFarmacia)
                            .addComponent(btn_GuardarFarmacia))
                        .addGap(40, 40, 40)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btn_EliminarFarmacia)
                            .addComponent(btn_ConsultarFarmacia)))
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 351, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(45, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Farmacia", jPanel1);

        jPanel2.setBackground(new java.awt.Color(51, 51, 51));
        jPanel2.setForeground(new java.awt.Color(102, 102, 102));

        Lbl_Nombre.setBackground(new java.awt.Color(255, 255, 255));
        Lbl_Nombre.setFont(new java.awt.Font("Roboto Medium", 3, 14)); // NOI18N
        Lbl_Nombre.setText("Nombre");

        Txt_Nombre.setForeground(new java.awt.Color(102, 102, 102));

        Lbl_ID.setBackground(new java.awt.Color(255, 255, 255));
        Lbl_ID.setFont(new java.awt.Font("Roboto Medium", 3, 14)); // NOI18N
        Lbl_ID.setText("ID");

        Txt_ID.setForeground(new java.awt.Color(102, 102, 102));

        Lbl_Temperatura.setFont(new java.awt.Font("Roboto Medium", 3, 14)); // NOI18N
        Lbl_Temperatura.setText("Temperatura");

        Txt_Temperatura.setForeground(new java.awt.Color(102, 102, 102));

        Lbl_ValorBase.setFont(new java.awt.Font("Roboto Medium", 3, 14)); // NOI18N
        Lbl_ValorBase.setText("Valor Base");

        Txt_ValorBase.setForeground(new java.awt.Color(102, 102, 102));

        Tb_Productos.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        Tb_Productos.setFont(new java.awt.Font("Roboto Black", 2, 12)); // NOI18N
        Tb_Productos.setForeground(new java.awt.Color(102, 102, 102));
        Tb_Productos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID", "Nombre", "Temperatura", "ValorBase", "Costo"
            }
        ));
        Tb_Productos.setGridColor(new java.awt.Color(0, 0, 204));
        jScrollPane1.setViewportView(Tb_Productos);

        btn_Actualizar.setBackground(new java.awt.Color(204, 204, 0));
        btn_Actualizar.setFont(new java.awt.Font("Roboto Black", 2, 12)); // NOI18N
        btn_Actualizar.setText("Actualizar");
        btn_Actualizar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btn_Actualizar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_ActualizarActionPerformed(evt);
            }
        });

        btn_Guardar.setBackground(new java.awt.Color(51, 204, 0));
        btn_Guardar.setFont(new java.awt.Font("Roboto Black", 2, 12)); // NOI18N
        btn_Guardar.setText("Guardar");
        btn_Guardar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btn_Guardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_GuardarActionPerformed(evt);
            }
        });

        btn_Eliminar.setBackground(new java.awt.Color(255, 0, 0));
        btn_Eliminar.setFont(new java.awt.Font("Roboto Black", 2, 12)); // NOI18N
        btn_Eliminar.setText("Eliminar");
        btn_Eliminar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btn_Eliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_EliminarActionPerformed(evt);
            }
        });

        btn_Consultar.setBackground(new java.awt.Color(0, 51, 255));
        btn_Consultar.setFont(new java.awt.Font("Roboto Black", 2, 12)); // NOI18N
        btn_Consultar.setText("Consultar");
        btn_Consultar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btn_Consultar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_ConsultarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(btn_Eliminar, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btn_Consultar, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(btn_Guardar, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btn_Actualizar, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(Lbl_ID, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(Lbl_Nombre, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(28, 28, 28))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(Lbl_Temperatura, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(Lbl_ValorBase, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(17, 17, 17)))
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(Txt_Nombre, javax.swing.GroupLayout.PREFERRED_SIZE, 141, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(Txt_ID, javax.swing.GroupLayout.PREFERRED_SIZE, 141, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(Txt_Temperatura, javax.swing.GroupLayout.PREFERRED_SIZE, 141, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(Txt_ValorBase, javax.swing.GroupLayout.PREFERRED_SIZE, 141, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 487, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(17, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap(47, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 351, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(8, 8, 8)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(Txt_ID, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(Lbl_ID))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(Txt_Nombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(Lbl_Nombre))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(Lbl_Temperatura)
                            .addComponent(Txt_Temperatura, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(Lbl_ValorBase)
                            .addComponent(Txt_ValorBase, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(31, 31, 31)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btn_Guardar)
                            .addComponent(btn_Actualizar))
                        .addGap(34, 34, 34)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btn_Eliminar)
                            .addComponent(btn_Consultar))))
                .addGap(26, 26, 26))
        );

        jTabbedPane1.addTab("Productos", jPanel2);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 773, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(16, 16, 16))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 450, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btn_ConsultarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_ConsultarActionPerformed
        Object[][] rowData = new Object[1][5];
        Object[] columnsNames = {"ID", "Nombre", "Temperatura", "ValorBase", "Costo"};
        DefaultTableModel model = new DefaultTableModel();
        model.setColumnIdentifiers(columnsNames);
        Tb_Productos.setModel(model);
        Producto p = new Producto();
        List<Producto> lista = p.listarProductos();
        for (Producto pro : lista) {
            rowData[0][0] = pro.getId();
            rowData[0][1] = pro.getNombre();
            rowData[0][2] = pro.getTemperatura();
            rowData[0][3] = pro.getValorBase();
            rowData[0][4] = pro.getCosto();
            model.addRow(rowData[0]);
        }
    }//GEN-LAST:event_btn_ConsultarActionPerformed

    private void btn_EliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_EliminarActionPerformed
        int id = Integer.parseInt(Txt_ID.getText());
        Producto p = new Producto();
        p.setId(id);
        if (p.eliminarProducto()) {            
            JOptionPane.showMessageDialog(this,"Operacion realizada con éxito");
            btn_ConsultarActionPerformed(evt);
        }
        else {
            JOptionPane.showMessageDialog(this, "Operacion fallida");
        }
    }//GEN-LAST:event_btn_EliminarActionPerformed

    private void btn_GuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_GuardarActionPerformed

        //int id = Integer.parseInt(Lbl_ID.getText());
        String nombre = Txt_Nombre.getText();
        double temperatura = Double.parseDouble(Txt_Temperatura.getText());
        double valorbase = Double.parseDouble(Txt_ValorBase.getText());

        Producto p = new Producto();

        //p.setId(id);
        p.setNombre(nombre);
        p.setTemperatura(temperatura);
        p.setValorBase(valorbase);
        p.setCosto(p.calcularCostoDeAlmacenamiento());
        if (p.guardarProducto()) {            
            JOptionPane.showMessageDialog(this, "Producto guardado con éxito\n"+ p.toString());
            btn_ConsultarActionPerformed(evt);
        }
        else {
            JOptionPane.showMessageDialog(this, "Error al guardar");
        }
    }//GEN-LAST:event_btn_GuardarActionPerformed

    private void btn_ActualizarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_ActualizarActionPerformed

        int id = Integer.parseInt(Txt_ID.getText());
        String nombre = Txt_Nombre.getText();
        double temperatura = Double.parseDouble(Txt_Temperatura.getText());
        double valorbase = Double.parseDouble(Txt_ValorBase.getText());

        Producto p = new Producto();
        p.setId(id);
        p.setNombre(nombre);
        p.setTemperatura(temperatura);
        p.setValorBase(valorbase);
        p.setCosto(p.calcularCostoDeAlmacenamiento());
        if (p.actualizarProducto()) {            
            JOptionPane.showMessageDialog(this, "Operacion realizada con éxito");
            btn_ConsultarActionPerformed(evt);
        }
        else {
            JOptionPane.showMessageDialog(this, "Operacion fallida");
        }
    }//GEN-LAST:event_btn_ActualizarActionPerformed
//-----------------------------------------------------------------------------------------
//----------------------BOTONES DE FARMACIA------------------------------------------------
//-----------------------------------------------------------------------------------------    
    
    private void btn_ConsultarFarmaciaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_ConsultarFarmaciaActionPerformed
        Object[][] rowData = new Object[1][5];
        Object[] columnsNames = {"ID", "Nit", "Nombre Farmacia", "Direccion", "Telefono"};
        DefaultTableModel model = new DefaultTableModel();
        model.setColumnIdentifiers(columnsNames);
        Tb_Farmacia.setModel(model);
        Farmacia f = new Farmacia();
        List<Farmacia> lista = f.ListaFarmacia();
        for (Farmacia farm : lista) {
            rowData[0][0] = farm.getId();
            rowData[0][1] = farm.getNit();
            rowData[0][2] = farm.getNombre();
            rowData[0][3] = farm.getDireccion();
            rowData[0][4] = farm.getTelefono();
            model.addRow(rowData[0]);
        }
    }//GEN-LAST:event_btn_ConsultarFarmaciaActionPerformed

    private void btn_EliminarFarmaciaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_EliminarFarmaciaActionPerformed
        int id = Integer.parseInt(Txt_IDFarmacia.getText());
        Farmacia f = new Farmacia();
        f.setId(id);
        if (f.eliminarFarmacia()) {            
            JOptionPane.showMessageDialog(this,"Operacion realizada con éxito");
            btn_ConsultarFarmaciaActionPerformed(evt);
        }
        else {
            JOptionPane.showMessageDialog(this, "Operacion fallida");
        }
    }//GEN-LAST:event_btn_EliminarFarmaciaActionPerformed

    private void btn_GuardarFarmaciaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_GuardarFarmaciaActionPerformed
        //int id = Integer.parseInt(Lbl_ID.getText());
        double nit = Double.parseDouble(Txt_NitFarmacia.getText());
        String nombre = Txt_Nombrefarmacia.getText();
        String direccion = Txt_DireccionFarmacia.getText();
        String telefono = Txt_Telefono.getText();

        Farmacia f = new Farmacia();

        //f.setId(id);
        f.setNit(nit);
        f.setNombre(nombre);        
        f.setDireccion(direccion);
        f.setTelefono(telefono);
        
        if (f.guardarFarmacia()) {            
            JOptionPane.showMessageDialog(this, "Producto guardado con éxito\n");
            btn_ConsultarFarmaciaActionPerformed(evt);
        }
        else {
            JOptionPane.showMessageDialog(this, "Error al guardar");
        }
    }//GEN-LAST:event_btn_GuardarFarmaciaActionPerformed

    private void btn_ActualizarFarmaciaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_ActualizarFarmaciaActionPerformed
        int id = Integer.parseInt(Txt_IDFarmacia.getText());
        double nit = Double.parseDouble(Txt_NitFarmacia.getText());
        String nombre = Txt_Nombrefarmacia.getText();
        String direccion = Txt_DireccionFarmacia.getText();
        String telefono = Txt_Telefono.getText();

        Farmacia f = new Farmacia();
        f.setId(id);
        f.setNit(nit);
        f.setNombre(nombre);
        f.setDireccion(direccion);
        f.setTelefono(telefono);
        if (f.actualizarFarmacia()) {            
            JOptionPane.showMessageDialog(this, "Operacion realizada con éxito");
            btn_ConsultarFarmaciaActionPerformed(evt);
        }
        else {
            JOptionPane.showMessageDialog(this, "Operacion fallida");
        }
    }//GEN-LAST:event_btn_ActualizarFarmaciaActionPerformed

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(JFrameProductos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(JFrameProductos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(JFrameProductos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(JFrameProductos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        
        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new JFrameProductos().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel Lbl_Direccion;
    private javax.swing.JLabel Lbl_ID;
    private javax.swing.JLabel Lbl_IDFarmacia;
    private javax.swing.JLabel Lbl_IDFarmacia1;
    private javax.swing.JLabel Lbl_Nfarmacia;
    private javax.swing.JLabel Lbl_Nombre;
    private javax.swing.JLabel Lbl_Temperatura;
    private javax.swing.JLabel Lbl_ValorBase;
    private javax.swing.JLabel Lbl_ValorBase1;
    private javax.swing.JTable Tb_Farmacia;
    private javax.swing.JTable Tb_Productos;
    private javax.swing.JTextField Txt_DireccionFarmacia;
    private javax.swing.JTextField Txt_ID;
    private javax.swing.JTextField Txt_IDFarmacia;
    private javax.swing.JTextField Txt_NitFarmacia;
    private javax.swing.JTextField Txt_Nombre;
    private javax.swing.JTextField Txt_Nombrefarmacia;
    private javax.swing.JTextField Txt_Telefono;
    private javax.swing.JTextField Txt_Temperatura;
    private javax.swing.JTextField Txt_ValorBase;
    private javax.swing.JToggleButton btn_Actualizar;
    private javax.swing.JToggleButton btn_ActualizarFarmacia;
    private javax.swing.JToggleButton btn_Consultar;
    private javax.swing.JToggleButton btn_ConsultarFarmacia;
    private javax.swing.JToggleButton btn_Eliminar;
    private javax.swing.JToggleButton btn_EliminarFarmacia;
    private javax.swing.JToggleButton btn_Guardar;
    private javax.swing.JToggleButton btn_GuardarFarmacia;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTabbedPane jTabbedPane1;
    // End of variables declaration//GEN-END:variables
}
